# Hi

I'm Jonah.

I am a Software Engineer currently working at Cisco on the [Cisco Secure Endpoint](https://www.cisco.com/c/en_ca/products/security/amp-for-endpoints/index.html) product.

Here's a list of *things* that I'm interested in and/or work with on the regular.

- **Software Development**
  - C and Modern C++
  - Container-based development environments
  - Docker
  - Git
  - Java
  - JavaScript
  - Linux
  - Python
  - Qt
- **Embedded Linux**
  - C/C++ for embedded systems
  - IoT architectures
  - IP networking
  - LTE/5G communications
  - Satellite communcations
  - Yocto Project
- **DevOps**
  - Amazon Web Services
  - CI/CD pipelines
  - GitLab
  - Terraform
- **Cybersecurity**
  - Embedded Linux security
  - Networking security
  - Web security
- **Outdoors**
  - Climbing
  - Cycling
  - Hiking
  - Snowboarding

## Education

Computer Science BSc (Hons) at Newcastle University, achieved a First Class (1st).

## Contact

Send mail to **jonahrobinson (at) duck (dot) com**
